﻿#include <iostream>
#include <math.h>

class Vector3D{
public:

    Vector3D():v("Default"),x(1),y(1),z(1)
    {
        std::cout << "Vector3D " << v << " [" << x << "," << y << "," << z <<"]"<< std::endl;
    };

    Vector3D(std::string _v, double _x, double _y, double _z) : v(_v),x(_x),y(_y),z(_z)
    {
        std::cout << "Vector3D " << v <<" [" << x << "," << y << "," << z << "]" << std::endl;
    }

    double getModuleV3D() {
    
        return sqrt(pow(x, 2)+pow(y, 2)+pow(z, 2));
    };

private:

    double x,y,z;
    std::string v;
};


int main()
{
    Vector3D V1;
    std::cout << "Vector3D V1 module = " << V1.getModuleV3D() << std::endl << std::endl;

    Vector3D V2("V2", 2, 3, 4);
    std::cout << "Vector3D V2 module = " << V2.getModuleV3D() << std::endl << std::endl;

    Vector3D V3("V3", 2, 1, 3);
    std::cout << "Vector3D V2 module = " << V3.getModuleV3D() << std::endl << std::endl;

    return 0;
}